import React, { useState, useEffect } from 'react';
import { getFighter } from '../../services/domainRequest/fightersRequest';
import FightLog from '../fightLog';
import { ListItemText } from '@material-ui/core';

const FightListElement = ({ fight, isOpened }) => {
  const [ fighter1, setFighter1 ] = useState('');
  const [ fighter2, setFighter2 ] = useState('');

  useEffect(() => {
    const fetchFightersNames = async id => {
      const { name: fighter1Name } = await getFighter(fight.fighter1);
      const { name: fighter2Name } = await getFighter(fight.fighter2);
      setFighter1(fighter1Name);
      setFighter2(fighter2Name);
    }

    fetchFightersNames();    
  }, [fight.fighter1, fight.fighter2]);

  return (
    <>
      <ListItemText>
        {fighter1} <strong>VS</strong> {fighter2}
      </ListItemText>
      {isOpened && <FightLog log={fight.log} />}
    </>
  );
}

export default FightListElement;
