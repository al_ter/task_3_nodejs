import * as React from 'react';
import SignInUpPage from '../signInUpPage';
import { isSignedIn } from '../../services/authService';
import Fight from '../fight';
import SignOut from '../signOut';
import Battle from '../battle';

class StartScreen extends React.Component {
  state = {
    isSignedIn: false,
    letsGetReadyToRumble: false,
    fighter1: null,
    fighter2: null
  };

  componentDidMount() {
    this.setIsLoggedIn(isSignedIn());
  }

  setFighters(fighter1, fighter2) {
    this.setState({ fighter1, fighter2 });
  }

  toggleRumble() {
    this.setState({
      letsGetReadyToRumble: !this.state.letsGetReadyToRumble
    })
  }

  setIsLoggedIn = (isSignedIn) => {
    this.setState({ isSignedIn });
  }

  render() {
    const { isSignedIn } = this.state;
    if (!isSignedIn) {
      return <SignInUpPage setIsLoggedIn={this.setIsLoggedIn} />
    }

    return (
      this.state.letsGetReadyToRumble ?
        <Battle
          fighter1={this.state.fighter1}
          fighter2={this.state.fighter2}
          toggleRumble={() => this.toggleRumble()}
        /> :
        <>
          <Fight
            setFighters={(fighter1, fighter2) => this.setFighters(fighter1, fighter2)}
            toggleRumble={() => this.toggleRumble()}
          />
          <SignOut isSignedIn={isSignedIn} onSignOut={() => this.setIsLoggedIn(false)} />
        </>
    );
  }
}

export default StartScreen;