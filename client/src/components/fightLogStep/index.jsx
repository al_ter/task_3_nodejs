import React from 'react';

const FightLogStep = ({ step }) => (
  <div className="fight-log-step">
    {
      Object.entries(step)
        .map(([key, value]) => `${key}: ${value}`)
        .join(', ')
    }
  </div>
);

export default FightLogStep;
