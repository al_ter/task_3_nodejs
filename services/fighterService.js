const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
  getAllFighters() {
    const fighters = FighterRepository.getAll();
    return fighters;
  }

  createFighter(data) {
    data.health = 100;

    if (data.name === 'IDKFA') {
      data.name = "Doomguy";
      data.power = 999;
    } else if (data.name === 'IDDQD') {
      data.name = "Doomguy";
      data.health = 9999;
      data.defense = 999;
    }

    const fighter = FighterRepository.create(data);

    if (!fighter) {
      return null;
    }
    
    return fighter;
  }

  updateFighter(fighterId, data) {
    const fighter = FighterRepository.update(fighterId, data);
    return fighter;
  }

  deleteFighter(fighterId) {
    const fighter = FighterRepository.delete(fighterId);
    return fighter;
  }

  search(search) {
    const fighter = FighterRepository.getOne(search);
    if (!fighter) {
      return null;
    }
    return fighter;
  }
}

module.exports = new FighterService();
