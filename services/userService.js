const { UserRepository } = require('../repositories/userRepository');

class UserService {
  getAllUsers() {
    const users = UserRepository.getAll();
    return users;
  }

  createUser(data) {
    const user = UserRepository.create(data);
    if (!user) {
      return null;
    }
    return user;
  }

  updateUser(userId, data) {
    const user = UserRepository.update(userId, data);
    return user;
  }

  deleteUser(userId) {
    const user = UserRepository.delete(userId);
    return user;
  }

  search(search) {
    const user = UserRepository.getOne(search);
    if (!user) {
      return null;
    }
    return user;
  }
}

module.exports = new UserService();
