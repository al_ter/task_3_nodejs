const { FightRepository } = require('../repositories/fightRepository');

class FightersService {
  getAllFights() {
    const fights = FightRepository.getAll();
    return fights;
  }

  createFight(data) {
    const fight = FightRepository.create(data);
    if (!fight) {
      return null;
    }
    return fight;
  }

  updateFight(fightId, data) {
    const fight = FightRepository.update(fightId, data);
    return fight;
  }

  deleteFight(fightId) {
    const fight = FightRepository.delete(fightId);
    return fight;
  }

  search(search) {
    const fight = FightRepository.getOne(search);
    if (!fight) {
      return null;
    }
    return fight;
  }
}

module.exports = new FightersService();
