// const FightService = require('../services/fightService');
const Joi = require('joi');

const fightSchema = Joi.object({
  fighter1: Joi.string().guid({ version: 'uuidv4' }),

  fighter2: Joi.string().guid({ version: 'uuidv4' }),

  log: Joi.array().items(Joi.object({
    fighter1Shot: Joi.number().min(0),
    fighter2Shot: Joi.number().min(0),
    fighter1Health: Joi.number().min(0),
    fighter2Health: Joi.number().min(0)
  }))
});

// const isFightExists = ({ id }) => FightService.search({ id }) ? true : false;

const fightValid = (req, res, next) => {
  try {
    const { error } = fightSchema.validate(req.body);

    if (error) {
      throw new Error(`Fight entity isn't valid`);
    }

    next();
  } catch (err) {
    res.status(400);
    next(err);
  }
}

exports.fightValid = fightValid;
