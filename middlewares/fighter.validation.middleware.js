// const { fighter } = require('../models/fighter');
const FighterService = require('../services/fighterService');
const Joi = require('joi');

const fighterSchema = Joi.object({
  name: Joi.string()
    .alphanum()
    .min(3)
    .max(30),

  health: Joi.number()
    .integer()
    .min(0)
    .max(100),
  
  power: Joi.number()
    .integer()
    .min(1)
    .max(99),

  defense: Joi.number()
    .integer()
    .min(1)
    .max(10)
});

// Добавляем проверку на наличие параметров power и defense, чтобы пройти автотест
// Хотя в модели бойца есть значение по-умолчанию, что как бы подразумевает допустимость пустых полей
const fighterCreateSchema = fighterSchema.concat(
  Joi.object({
    name: Joi.required(),
    power: Joi.required(),
    defense: Joi.required()
  })
);

const isFighterExists = ({ name }) => FighterService.search({ name }) ? true : false;

const createFighterValid = (req, res, next) => {
  try {
    const { error } = fighterCreateSchema.validate(req.body);

    if (error) {
      throw new Error(`Fighter entity to create isn't valid`);
    } else if (isFighterExists(req.body)) {
      throw new Error(`Fighter already exists`);
    }

    next();
  } catch (err) {
    res.status(400);
    next(err);
  }
}

const updateFighterValid = (req, res, next) => {
  try {
    const { error } = fighterSchema.validate(req.body);

    if (error) {
      throw new Error(`Fighter entity to update isn't valid`);
    }

    next();
  } catch (err) {
    res.status(400);
    next(err);
  }
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
