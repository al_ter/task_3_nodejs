const responseMiddleware = (req, res, next) => {
  if (res.data) {
    res.json(res.data);
    next();
  } else {
    next(new Error('Empty response'));
  }
}

exports.responseMiddleware = responseMiddleware;
