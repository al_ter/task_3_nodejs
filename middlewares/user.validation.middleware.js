// const { user } = require('../models/user');
const UserService = require('../services/userService');
const Joi = require('joi');

const phonePatern = /^\+380(39|67|68|96|97|98|50|66|95|99|63|73|93|91|92|94|70|80|90)[\d]{7}$/;
const emailPattern = /^[^@]+@gmail\.com$/;

const userSchema = Joi.object({
  firstName: Joi.string()
    .alphanum()
    .min(3)
    .max(30),

  lastName: Joi.string()
    .alphanum()
    .min(3)
    .max(30),

  password: Joi.string()
    .alphanum()
    .min(3)
    .max(30),

  phoneNumber: Joi.string()
    .pattern(phonePatern),

  email: Joi.string()
    .email({ minDomainSegments: 2, tlds: { allow: ['com'] } })
    .pattern(emailPattern)
});

const userCreateSchema = userSchema.concat(
  Joi.object({
    firstName: Joi.required(),
    lastName: Joi.required(),
    phoneNumber: Joi.required(),
    email: Joi.required(),
    password: Joi.required()
  })
);

const isUserExists = ({ email, phoneNumber}) => {
  if (UserService.search({ email }) || UserService.search({ phoneNumber })) {
    return true;
  }
  return false;
}

const createUserValid = (req, res, next) => {
  try {
    const { error } = userCreateSchema.validate(req.body);

    if (error) {
      throw new Error(`User entity to create isn't valid`);
    } else if (isUserExists(req.body)) {
      throw new Error(`User already exists`);
    }

    next();
  } catch (err) {
    res.status(400);
    next(err);
  }
}

const updateUserValid = (req, res, next) => {
  try {
    const { error } = userSchema.validate(req.body);

    if (error) {
      throw new Error(`User entity to update isn't valid`);
    }

    next();
  } catch (err) {
    res.status(400);
    next(err);
  }
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
