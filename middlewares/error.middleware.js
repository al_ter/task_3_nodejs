const errorMiddleware = (err, req, res, next) => {
  if (res.statusCode === 200) {
    res.status(400);
  }

  res.json({
    error: true,
    message: err.message
  });

  next();
}

exports.errorMiddleware = errorMiddleware;
