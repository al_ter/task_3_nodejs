const { Router } = require('express');
const FightService = require('../services/fightService');
const { fightValid } = require('../middlewares/fight.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.param('fightId', (req, res, next, id) => {
  try {
    const fight = FightService.search({ id });

    if (!fight) {
      throw new Error('Fight not found');
    }
    
    req.fight = fight;
    next();
  } catch (err) {
    res.status(404);
    next(err);
  }
});

router.route('/')
  .get((req, res, next) => {
    try {
      const fights = FightService.getAllFights();
      res.data = fights;
      next();
    } catch (err) {
      next(err);
    }
  })
  .post(fightValid, (req, res, next) => {
    try {
      const fight = FightService.createFight(req.body);
      res.data = fight;
      next();
    } catch (err) {
      next(err);
    }
  });

router.route('/:fightId')
  .get((req, res, next) => {
    if (req.fight) {
      res.data = req.fight;
      next();
    } else {
      res.status(404);
      next(new Error('Fight not found'));
    }
  })
  .put(fightValid, (req, res, next) => {
    try {
      const fight = FightService.updateFight(req.fight.id, req.body);
      res.data = fight;
      next();
    } catch (err) {
      next(err);
    }
  })
  .delete((req, res, next) => {
    try {
      const fight = FightService.deleteFight(req.fight.id);
      res.data = fight;
      next();
    } catch (err) {
      next(err);
    }
  });

router.use(responseMiddleware);

module.exports = router;
