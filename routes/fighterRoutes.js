const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
// const { errorMiddleware } = require('../middlewares/error.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.param('fighterId', (req, res, next, id) => {
  try {
    const fighter = FighterService.search({ id });
    
    if (!fighter) {
      throw new Error('Fighter not found');
    }
    
    req.fighter = fighter;
    next();
  } catch (err) {
    res.status(404);
    next(err);
  }
});

router.route('/')
  .get((req, res, next) => {
    try {
      const fighters = FighterService.getAllFighters();
      res.data = fighters;
      next();
    } catch (err) {
      next(err);
    }
  })
  .post(createFighterValid, (req, res, next) => {
    try {
      const fighter = FighterService.createFighter(req.body);
      res.data = fighter;
      next();
    } catch (err) {
      next(err);
    }
  });

router.route('/:fighterId')
  .get((req, res, next) => {
    if (req.fighter) {
      res.data = req.fighter;
      next();
    } else {
      res.status(404);
      next(new Error('Fighter not found'));
    }
  })
  .put(updateFighterValid, (req, res, next) => {
    try {
      const fighter = FighterService.updateFighter(req.fighter.id, req.body);
      res.data = fighter;
      next();
    } catch (err) {
      next(err);
    }
  })
  .delete((req, res, next) => {
    try {
      const fighter = FighterService.deleteFighter(req.fighter.id);
      res.data = fighter;
      next();
    } catch (err) {
      next(err);
    }
  });

router.use(responseMiddleware);

module.exports = router;
