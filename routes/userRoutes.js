const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');
// const { errorMiddleware } = require('../middlewares/error.middleware');
// const { route } = require('./authRoutes');

const router = Router();

router.param('userId', (req, res, next, id) => {
  try {
    const user = UserService.search({ id });

    if (!user) {
      throw new Error('User not found');
    }

    req.user = user;
    next();
  } catch (err) {
    res.status(404);
    next(err);
  }
});

router.route('/')
  .get((req, res, next) => {
    try {
      const users = UserService.getAllUsers();
      res.data = users;
      next();
    } catch (err) {
      next(err);
    }
  })
  .post(createUserValid, (req, res, next) => {
    try {
      const user = UserService.createUser(req.body);
      res.data = user;
      next();
    } catch (err) {
      next(err);
    }
  });

router.route('/:userId')
  .get((req, res, next) => {
    if (req.user) {
      res.data = req.user;
      next();
    } else {
      res.status(404);
      next(new Error('User not found'));
    }
  })
  .put(updateUserValid, (req, res, next) => {
    try {
      const user = UserService.updateUser(req.user.id, req.body);
      res.data = user;
      next();
    } catch (err) {
      next(err);
    }
  })
  .delete((req, res, next) => {
    try {
      const user = UserService.deleteUser(req.user.id);
      res.data = user;
      next();
    } catch (err) {
      next(err);
    }
  });

router.use(responseMiddleware);

module.exports = router;
