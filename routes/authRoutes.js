const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { errorMiddleware } = require('../middlewares/error.middleware');

const router = Router();

router.post('/login', (req, res, next) => {
  try {
    const user = AuthService.login(req.body);
    res.data = user;
    next();
  } catch (err) {
    res.status(404);
    next(err);
  }
});

router.use(responseMiddleware, errorMiddleware);

module.exports = router;
